<?php

class UserTest extends TestCase
{

    public $id = 5;

    /**
     * /users [GET]
     */
    public function testAllUsersResponseStructure()
    {
        $this->get("users", [])->getResult();
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            [
                [
                    'id',
                    'firstname',
                    'lastname',
                    'email',
                    'event_id',
                    'created_at',
                    'updated_at',
                ]
            ]
        );
    }

    /**
     * /users/{id} [GET]
     */
    public function testUserStructure()
    {
        $this->get("users/" . $this->id, []);
        $this->seeStatusCode(200);
        $this->seeJson(
            [
                'id' => $this->id,
            ]
        );
        $this->seeJsonStructure(
            [
                'id',
                'firstname',
                'lastname',
                'email',
                'event_id',
                'created_at',
                'updated_at'
            ]
        );
    }


    /**
     * /users/{id} [GET]
     */
    public function testUserEntity()
    {
        $this->get("users/" . $this->id, []);
        $this->seeStatusCode(200);
        $this->seeJson(
            [
                'id' => $this->id,
            ]
        );
    }

    /**
     * /users [POST]
     */
    public function testCreateUser()
    {
        $firstname = 'Hello';
        $lastname = 'UnitTest';
        $event_id = rand(1, 10);

        $parameters = [
            'firstname' => $firstname,
            'lastname'  => $lastname,
            'email'     => 'unittest@gmail.com',
            'event_id'  => $event_id
        ];

        $this->post("users", $parameters, []);
        $this->seeStatusCode(201);
        $this->seeJson(
            [
                'firstname' => $firstname,
                'lastname'  => $lastname,
                'event_id'  => $event_id,
                'email'     => 'unittest@gmail.com'
            ]
        );

    }

    /**
     * /users/id [PUT]
     */
    public function testUpdateUser()
    {
        $new_value = 'UnitTest';
        $parameters = [
            'firstname' => $new_value,
        ];

        $this->put("users/" . $this->id, $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJson(
            [
                'firstname' => $new_value,
            ]
        );
    }

    /**
     * /products/id [DELETE]
     */
    public function testDeleteUser()
    {
        $this->delete("users/" . $this->id, [], []);
        $this->seeStatusCode(410);
        $this->seeJsonEquals(
            [
                'message' => "Successfully deleted"
            ]
        );
    }
}
