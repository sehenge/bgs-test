# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Installation steps

> git clone https://gitlab.com/sehenge/bgs-test.git
>
> cd bgs-test
>
> chmod -R 777 storage
>
> cp .env.testing .env
>
> docker-compose up -d
>
> docker-compose exec app bash
>
> composer install
>
> php artisan migrate --seed

App will be available on http://localhost

## API endpoints
GET REQUESTS:
> http://localhost/users - shows you all users
>
> http://localhost/users/5 - get users appended to event with id 5
> 
> http://localhost/events - get all events
> 
> http://localhost/events/5 - get event with id 5
> 
POST REQUEST:
> http://localhost/users - add user
>
> Example POST body:
```json
{
  "firstname": "Test",
  "lastname": "Test",
  "email": "test@hotmail.com",
  "event_id": 4
}
```
PUT REQUEST:
> http://localhost/users/2 - update user info with id 2
```json
{
  "firstname": "Test 2",
  "email": "test2@hotmail.com",
  "event_id": 5
}
```
DELETE REQUEST:
> http://localhost/users/2 - delete user with id 2

