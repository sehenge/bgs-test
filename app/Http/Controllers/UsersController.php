<?php

namespace App\Http\Controllers;

use App\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(Users::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $log = new Logger('api');
        $log->pushHandler(new StreamHandler(storage_path() . '/logs/api.log', Logger::INFO));

        $rules = [
            'firstname' => 'required',
            'lastname' => 'required',
            'event_id' => 'required',
            'email' => 'required',
        ];
        $this->validate($request, $rules);


        try {
            $user = Users::create($request->all());
            $log->info('Created new user: ' . $user);

//            TODO: setup it if necessary
//            Mail::send('emails.reminder', ['user' => $user], function ($m) use ($user) {
//                $m->from('sender@app.com', 'Your Application');//
//                $m->to('receiver@gmail.com', $user->firstname)->subject('Your Reminder!');

//                $m->queue();
//            });
        } catch (\Exception $exception) {
            $log->error('Cannot create new user: ' . $exception);

            return response()->json($exception->errorInfo, 409);
        }

        return response()->json($user, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return response()->json(Users::find($id), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $user = Users::findOrFail($id);
        $user->update($request->all());

        return response()->json($user, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        Users::findOrFail($id)->delete($id);

        return response()->json(['message' => "Successfully deleted"], 410);
    }
}
