<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'city', 'date'
    ];

    public function getEventUsers(int $event_id)
    {
        $event = $this->where('events.id', '=', $event_id)
            ->join('users', 'users.event_id', '=', 'events.id')
            ->get(['name', 'city', 'date', 'firstname', 'lastname', 'email']);


        $result['event']['name'] = $event->first()->name;
        $result['event']['city'] = $event->first()->city;
        $result['event']['date'] = $event->first()->date;

        foreach ($event as $item) {
            $users['first_name'] = $item->firstname;
            $users['last_name'] = $item->lastname;
            $users['email'] = $item->email;
            $result['users'][] = $users;
        }


        return response()->json($result)->getData();
    }
}
