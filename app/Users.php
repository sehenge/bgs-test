<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'event_id', 'email'
    ];

    public function getEventUsers(int $event_id): string
    {
        return $this->where('event_id', '=', $event_id)->get();
    }
}
